package biz.agvcorp.mysqlintegration;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

/**
 * Created by Shazzad on 1/16/2018.
 */

public class ProfileDrawerFragment extends Fragment implements DatePickerDialog.OnDateSetListener{
    private AppCompatActivity mActivity;

    View mView;
    Map<String, String> profileData;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    private View mProgressView;
    private View mProfileFormView;

    private EditText mFirstNameView;
    private EditText mLastNameView;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mNidView;
    private EditText mPhoneNumberView;
    private EditText mAddressView;
    private EditText mDateOfBirthView;
    private AppCompatButton mProfileButton;

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mProfileFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);}
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);}
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.profile_activity_drawer, container, false);

        pref = mView.getContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        mProfileFormView = mView.findViewById(R.id.profile_form);
        mProgressView = mView.findViewById(R.id.profile_progress);
        mProfileButton = mView.findViewById(R.id.btn_saveProfile);
        mProfileButton.setEnabled(false);

        return mView;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showProgress(true);

        mFirstNameView = view.findViewById(R.id.firstname);
        mLastNameView = view.findViewById(R.id.lastname);
        mEmailView = view.findViewById(R.id.email);
        mPasswordView = view.findViewById(R.id.password);
        mNidView = view.findViewById(R.id.nid);
        mPhoneNumberView = view.findViewById(R.id.phone);
        mAddressView = view.findViewById(R.id.address);
        mDateOfBirthView = view.findViewById(R.id.dob);

        mDateOfBirthView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newDateDialogueFragment = new SelectDateFragment();
                newDateDialogueFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        final String url = getString(R.string.host_url)+"/"+getString(R.string.profile_view)+"/"+getCurrentId().toString();
        Log.d("Current Url", url);

        RequestQueue queue = Volley.newRequestQueue(view.getContext());
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                showProgress(false);
                Log.d("ProfileResponse", response.toString());
//                mFirstNameView.setText("This is a Test Test test");
                try {
                    profileData = new HashMap<String, String>();
                    profileData.put("id",response.getString("id"));
                    profileData.put("firstname",response.getString("first_name"));
                    profileData.put("lastname", response.getString("last_name"));
                    profileData.put("email",response.getString("email"));
                    profileData.put("password", response.getString("password"));
                    profileData.put("nid",response.getString("nid"));
                    profileData.put("phone", response.getString("mobile"));
                    profileData.put("dob", response.getString("dob"));
                    profileData.put("address", response.getString("address"));

                    mFirstNameView.setText( response.getString("first_name") );  //NULL AITASE
                    mLastNameView.setText(response.getString("last_name"));
                    mEmailView.setText(response.getString("email"));
                    mPasswordView.setText("");
                    mPasswordView.setTypeface(Typeface.DEFAULT);
                    mPasswordView.setTransformationMethod(new PasswordTransformationMethod());
                    mNidView.setText(response.getString("nid"));
                    mPhoneNumberView.setText( response.getString("mobile") );
                    mAddressView.setText( response.getString("address") );
                    mDateOfBirthView.setText( response.getString("dob") );

//                    if (response.getString("first_name"). ){
//                        mFirstNameView.setText( response.getString("first_name") );
//                    }
//                    if (response.getString("last_name").equals(null)){
//                        mLastNameView.setText(response.getString("last_name"));
//                    }
//                    if (response.getString("email").equals(null)){
//                        mEmailView.setText(response.getString("email"));
//                    }
//                    mPasswordView.setText("");
//                    mPasswordView.setTypeface(Typeface.DEFAULT);
//                    mPasswordView.setTransformationMethod(new PasswordTransformationMethod());
//                    if (response.getString("nid").equals(null)){
//                        mNidView.setText(response.getString("nid"));
//                    }
//                    if (response.getString("mobile").equals(null)){
//                        mPhoneNumberView.setText( response.getString("mobile") );
//                    }
//                    if (response.getString("address").equals(null)){
//                        mAddressView.setText( response.getString("address") );
//                    }
//                    if (response.getString("dob").equals(null)){
//                        mDateOfBirthView.setText( response.getString("dob") );
//                    }

                    //showProgress(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                    showProgress(false);
                    Toast.makeText(view.getContext(), "SERVER OFFLINE. TRY AGAIN", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                Toast.makeText(view.getContext(), "SERVER OFFLINE. TRY AGAIN", Toast.LENGTH_LONG).show();
            }
        });
        queue.add(postRequest);

        mProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Map<String, String> updateProfileData = new HashMap<String, String>();
                updateProfileData.put("firstname", mFirstNameView.getText().toString());
                updateProfileData.put("lastname", mLastNameView.getText().toString());
                updateProfileData.put("email", mEmailView.getText().toString());
                updateProfileData.put("password", mPasswordView.getText().toString());
                updateProfileData.put("nid",mNidView.getText().toString());
                updateProfileData.put("phone", mPhoneNumberView.getText().toString());
                updateProfileData.put("address", mAddressView.getText().toString());
                updateProfileData.put("dob", mDateOfBirthView.getText().toString());

                final String updateUrl = getString(R.string.host_url)+"/"+getString(R.string.profile_update)+"/"+getCurrentId().toString();

                RequestQueue profilequeue = Volley.newRequestQueue(view.getContext());
                JsonObjectRequest postProfileRequest = new JsonObjectRequest(Request.Method.POST, updateUrl, new JSONObject(updateProfileData), new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("ProfileUpdateResponse", response.toString());
                        try {
                            if (response.getBoolean("status")){
                                Toast.makeText(view.getContext(), "Profile Updated Successfully!", Toast.LENGTH_LONG).show();
                            } else {
                                showProgress(false);
                                Toast.makeText(view.getContext(), "Update Failed"+response.getBoolean("message"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(view.getContext(), "JSON Exception"+e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        Toast.makeText(view.getContext(), "Response Error Listener"+error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
                profilequeue.add(postProfileRequest);
            }
        });
    }

    private String getCurrentId(){
        return pref.getString("logginID", "");
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }
}
