package biz.agvcorp.mysqlintegration;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    private String currentId;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mTextView = findViewById(R.id.demo);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        if (getCurrentId() != ""){
            Toast.makeText(getApplicationContext(), "Current Logged in User is "+getCurrentId(), Toast.LENGTH_LONG).show();
            mTextView.setText("Current Logged in User is "+getCurrentId());
        } else {
            mTextView.setText("No User Logged In!");
        }

    }

    private Boolean storeInSP(){
        editor.putString("logginID", currentId); // Storing string
//        editor.putBoolean("key_name", true); // Storing boolean - true/false
//        editor.putInt("key_name", "int value"); // Storing integer
//        editor.putFloat("key_name", "float value"); // Storing float
//        editor.putLong("key_name", "long value"); // Storing long

        editor.commit(); // commit changes
        return true;
    }

    private String getCurrentId(){
        return pref.getString("logginID", "");
    }
}
