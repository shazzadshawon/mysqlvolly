package biz.agvcorp.mysqlintegration;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.READ_CONTACTS;
import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class SignUpActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    //    private final String uri = "http://192.168.10.123/androidMysql/api/login";
    private final String uri = "http://192.168.10.140/projects/android_api/api/register";

    private static final int REQUEST_READ_CONTACTS = 0;
    private SignUpActivity.UserRegistrationTask mAuthTask = null;

    private EditText mFirstNameView;
    private EditText mLastNameView;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mNidView;
    private EditText mPhoneNumberView;

    private View mProgressView;
    private View mRegistrationFormView;

    private Button mEmailSignInButton;
    private TextView mSignInLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mEmailView = findViewById(R.id.email);
        populateAutoComplete();

        mPasswordView = findViewById(R.id.password);
        mFirstNameView = findViewById(R.id.firstname);
        mLastNameView = findViewById(R.id.lastname);
        mNidView = findViewById(R.id.nid);
        mPhoneNumberView = findViewById(R.id.phone);


        mEmailSignInButton = findViewById(R.id.btn_signup);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegistration();
            }
        });

        mRegistrationFormView = findViewById(R.id.registration_form);
        mProgressView = findViewById(R.id.registration_progress);

        mSignInLink = findViewById(R.id.link_login);
        mSignInLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(signInIntent);
                finish();
            }
        });
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    private void attemptRegistration() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mFirstNameView.setError(null);
        mLastNameView.setError(null);
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mNidView.setError(null);
        mPhoneNumberView.setError(null);

        // Store values at the time of the registration attempt.
        String firstname = mFirstNameView.getText().toString();
        String lastname = mLastNameView.getText().toString();
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String nid = mNidView.getText().toString();
        String phone = mPhoneNumberView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        /*
        * Validation
        * Validation
        * Validation
        * */

        // Check for a valid firstName, if the user entered one.
        if (TextUtils.isEmpty(firstname)) {
            mFirstNameView.setError(getString(R.string.error_field_required));
            focusView = mFirstNameView;
            cancel = true;
        } else if (!isFirstNameValid(firstname)) {
            mFirstNameView.setError(getString(R.string.error_invalid_firstname));
            focusView = mFirstNameView;
            cancel = true;
        }

        // Check for a valid lastName, if the user entered one.
        if (TextUtils.isEmpty(lastname)) {
            mLastNameView.setError(getString(R.string.error_field_required));
            focusView = mLastNameView;
            cancel = true;
        } else if (!isLastNameValid(lastname)) {
            mLastNameView.setError(getString(R.string.error_invalid_lastname));
            focusView = mLastNameView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        // Check for a valid NID address.
        if (TextUtils.isEmpty(nid)) {
            mNidView.setError(getString(R.string.error_field_required));
            focusView = mNidView;
            cancel = true;
        } else if (!isNidValid(nid)) {
            mNidView.setError(getString(R.string.error_invalid_nid));
            focusView = mNidView;
            cancel = true;
        }

        // Check for a valid Phone Number.
        if (TextUtils.isEmpty(phone)) {
            mPhoneNumberView.setError(getString(R.string.error_field_required));
            focusView = mPhoneNumberView;
            cancel = true;
        } else if (!isPhoneValid(phone)) {
            mPhoneNumberView.setError(getString(R.string.error_invalid_phonenunber));
            focusView = mPhoneNumberView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
//            mAuthTask = new UserRegistrationTask(email, password);
            mAuthTask = new UserRegistrationTask(firstname, lastname, email, password, nid, phone);
            mAuthTask.execute((Void) null);
        }

        /*
        * Validation
        * Validation
        * Validation
        * */
    }

    private boolean isPhoneValid(String phone) {
        if (phone.length() > 11){
            return false;
        } return true;
    }

    private boolean isNidValid(String nid) {
        if (nid.length() > 14){
            return false;
        } return true;
    }

    private boolean isLastNameValid(String lastname) {
        return true;
    }

    private boolean isFirstNameValid(String firstname) {
        return true;
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRegistrationFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), SignUpActivity.ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(SignUpActivity.ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(SignUpActivity.this, android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */

    public class UserRegistrationTask extends AsyncTask<Void, Void, Boolean> {


        private String mFirstName;
        private String mLastName;
        private String mNid;
        private String mPhone;
        private final String mEmail;
        private final String mPassword;

        private Boolean successFalg = false;

        UserRegistrationTask(String firstname, String lastname, String email, String password, String nid, String phone) {
            mFirstName = firstname;
            mLastName = lastname;
            mEmail = email;
            mPassword = password;
            mNid = nid;
            mPhone = phone;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // Code For Volly
            RequestQueue queue = Volley.newRequestQueue(SignUpActivity.this);

            Map<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("firstname",mFirstName);
            jsonParams.put("lastname", mLastName);
            jsonParams.put("email",mEmail);
            jsonParams.put("password", mPassword);
            jsonParams.put("nid",mNid);
            jsonParams.put("phone", mPhone);

            JsonObjectRequest postRequest = new JsonObjectRequest
                    (Request.Method.POST, uri, new JSONObject(jsonParams), new Response.Listener<JSONObject>() {

                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Response", response.toString());
                            try {
                                if (response.getBoolean("status")){
                                    showProgress(false);
                                    Toast.makeText(getApplicationContext(), "Registration Success", Toast.LENGTH_LONG).show();
                                    successFalg = true;
                                    jumpWelcomeIntent();
                                } else {
                                    showProgress(false);
                                    Toast.makeText(getApplicationContext(), "Registration Failed", Toast.LENGTH_LONG).show();
                                    successFalg = false;
                                }
                            } catch (JSONException e) {
                                showProgress(false);
                                Toast.makeText(getApplicationContext(), "SERVER BUSY TRY AGAIN", Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            showProgress(false);
                            Toast.makeText(getApplicationContext(), "SERVER BUSY TRY AGAIN", Toast.LENGTH_LONG).show();
                            // TODO Auto-generated method stub

                        }
                    });
            queue.add(postRequest);

            // TODO: register the new account here.
            return successFalg;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Boolean success) {
            mAuthTask = null;
/*            showProgress(false);
            Toast.makeText(getApplicationContext(), "Runnes after the Respnse arrives", Toast.LENGTH_SHORT).show();
            if (success) {
                Intent homeIntent = new Intent(SignInActivity.this, HomeActivity.class);
                homeIntent.putExtra(EXTRA_MESSAGE, "You are SuccessFully Logged In");
                startActivity(homeIntent);
                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }*/
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private void jumpWelcomeIntent(){
        Intent homeIntent = new Intent(SignUpActivity.this, HomeActivityDrawer.class);
        homeIntent.putExtra(EXTRA_MESSAGE, "You are SuccessFully Logged In");
        startActivity(homeIntent);
        finish();
    }
}